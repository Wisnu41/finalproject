import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Icon from "@expo/vector-icons/MaterialCommunityIcons";

export default function ProfileScreen() {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 36, letterSpacing: 2 }}>Profile</Text>
      <Image source={require('../assets/photo.jpg')} style={{ height: 120, width: 120, borderRadius: 60, marginVertical: 20 }} />
      <View style={styles.listContainer}>
        <View style={styles.list}>
          <Text style={{ fontSize: 18 }}>name</Text>
          <Text style={styles.listText}>Wisnu Murfadilah R</Text>
        </View>
        <View style={styles.list}>
          <Text style={{ fontSize: 18 }}>job</Text>
          <Text style={styles.listText}>Web Developer</Text>
        </View>
        <View style={styles.list}>
          <Icon name="telegram" size={45} />
          <Text style={styles.listText}>@whis41</Text>
        </View>
        <View style={styles.list}>
          <Icon name="email" size={45} />
          <Text style={styles.listText}>wisnukm41@gmail.com</Text>
        </View>
        <View style={styles.list}>
          <Icon name="gitlab" size={45} />
          <Text style={styles.listText}>@Wisnu41</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  listContainer: {
    borderColor: "#000",
    borderWidth: 1,
    padding: 15
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 280,
    marginBottom: 12
  },
  listText: {
    alignSelf: "flex-end",
    fontSize: 20
  }
})
