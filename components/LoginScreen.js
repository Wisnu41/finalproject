import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';

export default function LoginScreen(props) {

  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState();
  const [isError, setisError] = useState(false);

  function loginHandler() {
    if (userName === "wisnukm41" && password === "12345678") {
      props.navigation.navigate("Home")
    } else {
      setisError(true)
      setPassword('')
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.textSignIn}>
        Sign In
      </Text>
      <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
          <Text style={styles.inputLabel}>Username</Text>
          <TextInput
            style={styles.inputText}
            onChangeText={userName => setUsername(userName)}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.inputLabel}>Password</Text>
          <TextInput
            style={styles.inputText}
            secureTextEntry={true}
            onChangeText={password => setPassword(password)}
            value={password}
          />
        </View>
        <Text style={isError ? styles.errorText : styles.hiddenErrorText}>Username Atau Password Salah</Text>
        <TouchableOpacity style={styles.button} onPress={loginHandler}>
          <Text style={{ fontSize: 20, color: "#fff" }}>
            Sign in
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  textSignIn: {
    fontSize: 36
  },
  formContainer: {
    marginTop: 50
  },
  inputContainer: {
    marginBottom: 35
  },
  inputLabel: {
    fontSize: 18
  },
  inputText: {
    fontSize: 16,
    width: 280,
    borderColor: "#7994F5",
    borderWidth: 2,
    height: 45,
    paddingHorizontal: 10,
    backgroundColor: "#EEEEF3"
  },
  button: {
    backgroundColor: "#7994F5",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 20,
    width: 200,
    alignSelf: "center",
    elevation: 5,
    borderRadius: 35,
    marginTop: 40
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
  }
})