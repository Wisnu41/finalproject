import React from 'react';
import { View, Text } from 'react-native';

import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import ProfileScreen from './ProfileScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function index() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
    </Drawer.Navigator>
  )
}

function Home() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Feed" component={HomeScreen} />
      <Stack.Screen name="News Details" component={DetailScreen} />
    </Stack.Navigator>
  )
}


