import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';

export default function DetailScreen(props) {
  const data = props.route.params.data;
  const uri = data.urlToImage;

  function formatDate(date) {
    const formatted = String(date).substr(0, 10).split('-');
    return `${formatted[2]} / ${formatted[1]} / ${formatted[0]}`
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.newsTitle}>{data.title}</Text>
        <Image source={{ uri }} style={styles.newsImage} />
        <View style={styles.newsInfo}>
          <Text style={{ width: 180 }}>{data.author}</Text>
          <Text>{formatDate(data.publishedAt)}</Text>
        </View>
        <Text style={styles.newsContent}>
          {data.content}
        </Text>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  newsTitle: {
    alignSelf: "flex-start",
    fontSize: 20,
    marginTop: 40,
    paddingLeft: 15,
    marginBottom: 10
  },
  newsImage: {
    width: 320,
    maxHeight: 250,
    minHeight: 200
  },
  newsInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 320,
    paddingVertical: 10,
    borderBottomColor: "#000",
    borderBottomWidth: 1,
  },
  newsContent: {
    textAlign: "justify",
    marginVertical: 6,
    marginHorizontal: 20
  }
})

