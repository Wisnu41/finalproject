import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, FlatList } from 'react-native';
import axios from 'axios';

import LoadingScreen from './LoadingScreen';

export default function HomeScreen(props) {

  const [news, setNews] = useState([{}]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await axios.get('https://newsapi.org/v2/top-headlines?country=id&apiKey=eb6481aa5bf144f79fdb08928932a8b5').
          then(response => {
            setNews(response.data.articles)
            setIsLoading(false)
          }).catch(error => { console.log(error) })
      } catch (e) {
      }
    }
    fetchData()
  }, []);

  if (isLoading) return (<LoadingScreen />)

  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 36 }}>News</Text>
      <FlatList data={news} keyExtractor={(item, i) => i} renderItem={({ item }) => <NewsItem navigation={props.navigation} data={item} />} />
    </View>
  )
}

function NewsItem(props) {
  return (
    <TouchableOpacity onPress={() => props.navigation.navigate("News Details", { data: props.data })} style={{ marginBottom: 10 }}>
      <ImageBackground source={{ uri: props.data.urlToImage }} style={styles.newsImage}>
        <Text style={styles.newsTitle}>{props.data.title}</Text>
      </ImageBackground>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  newsTitle: {
    fontSize: 16,
    padding: 10,
    backgroundColor: "rgba(255,255,255,0.7)",
  },
  newsImage: {
    width: 320,
    height: 230,
    justifyContent: "flex-end",
  }
})
